﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Client
{
    public partial class Form1 : Form
    {
        static private string delimiter = ";;;S";
        static private int DEFAULT_BUFLEN = 2048;
        static private int port = 7770;

        static private bool is_disconected = false;
        static private Socket Client;
        private IPAddress ip = null;
        private Thread th;

        public Form1()
        {
            InitializeComponent();

            richTextBox1.Enabled = true;
            richTextBox2.Enabled = false;
            button1.Enabled = false;

            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                SetInfo(Color.Green, "Настройки: \nПодключение к " + ip.ToString() + ":" + port.ToString());
            }
            catch (Exception ex)
            {
                SetInfo(Color.Red, "Настройки не найдены\n" + "Ошибка: " + ex.Message);
                ShowSett();
            }

        }

        void init()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);

                button1.Enabled = true;
                richTextBox2.Enabled = true;
                niknameField.Enabled = false;
                button2.Enabled = false;
                SetInfo(Color.Green, "Настройки: \nПодключено к " + ip.ToString() + ":" + port.ToString());

                th = new Thread(delegate () { RecvMessage(); }); th.Start();
            }
        }

        void SetInfo(Color cl, string text){
            label4.ForeColor = cl;
            label4.Text = text;
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        void SendMessage(string message)
        {
            if (message.Trim() == "" || !Client.Connected)
                return;

            byte[] buffer = new byte[1024];
            buffer = Encoding.UTF8.GetBytes(message);
            Client.Send(buffer);
        }

        void SetDisconected()
        {
            label4.ForeColor = Color.Red;
            label4.Text = "Disconected";
        }


        void RecvMessage()
        {
            byte[] buffer = new byte[1024];

            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(delimiter);
                        if (count == -1)
                        {
                            continue;
                        }

                        string clear_message = "";

                        for (int i = 0; i < count; i++)
                        {
                            clear_message += message[i];
                        }
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            buffer[i] = 0;
                        }

                        this.Invoke((MethodInvoker)delegate ()
                        {
                            richTextBox1.AppendText(clear_message);
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }
 
        private void button2_Click(object sender, EventArgs e)
        {
            if(niknameField.Text.Trim() != "")
            {
                try{
                    init();
                } catch (Exception ex){
                    SetInfo(Color.Red, ex.Message);
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void BeforeClose()
        {
            if (th != null) th.Abort();
            if (Client != null) Client.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendMessage("\n" + niknameField.Text + ": " + richTextBox2.Text.Trim() + delimiter);
            richTextBox2.Clear();
        }

        private void авторToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n 2018");
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
           // Application.Exit();
        }
    }
}
