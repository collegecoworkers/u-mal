#include "main.hpp"

int main(int argc, char const *argv[]) {

  setlocale(LC_ALL, "russian");

  WSAData data;
  WORD version = MAKEWORD(2, 2);
  int res = WSAStartup(version, &data);
  
  if (res != 0) return 0;

  struct addrinfo hints;
  struct addrinfo *result;
  struct addrinfo *ptr;
  int rv; // Error checking on getaddrinfo

  ZeroMemory(&hints, sizeof(hints));

  hints.ai_family = AF_INET;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if ((rv = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result)) != 0) {
  	fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
  	cleanup();
  	exit(1);
  }

  for (ptr = result; ptr != NULL; ptr = ptr->ai_next){
  	if ((Listen = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol)) == -1) {
  		perror("listener: socket");
  		continue;
  	}
  	if (bind(Listen, ptr->ai_addr, ptr->ai_addrlen) == -1){
  		closesocket(Listen);
  		perror("listener: bind");
  	}
  	break;
  }

  if (ptr == NULL){
  	fprintf(stderr, "listener: failed to bind socket\n");
  	exit(2);
  }

  printNetworkAddress();

  listen(Listen, SOMAXCONN);

  freeaddrinfo(result);

  puts("Start server...");
  
  for (;; Sleep(75)) {
    if (Connect = accept(Listen, NULL, NULL)) {
      Connections.push_back(new ItemConnect(Connect));
      int ID = Connections.size() - 1;
      Connections[ID]->sendMessage("Connect...");
      puts("Connection init: client " << ID );
      hThreads[ID] = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)SendMessToClient, (LPVOID)(ID), NULL, NULL);
    }
  }

  closesocket(Listen);
  cleanup();

  return 1;
}

ItemConnect::ItemConnect(SOCKET c){
  this->name = "PK " + to_str(c);
  this->connect = c;
}
void ItemConnect::close(){
  this->is_closed = true;
  closesocket(this->connect);
}
void ItemConnect::sendMessage(const str_t& buffer){
  if (this->is_closed) return;
  str_t new_m(buffer);
  new_m += DELIMITER;
  send(this->connect, new_m.c_str(), new_m.length(), NULL);
  new_m.clear();
}
ItemConnect::~ItemConnect(){}


void cleanup(){
    WSACleanup();
}

str_t clearMessage(const str_t& mess) {
  std::size_t found = mess.find(DELIMITER);

  if (found == str_npos) return "error commad";

  str_t clear_message = "";
  for (size_t i = 0; i < found; i++)
    clear_message += mess[i];

  return clear_message;
}

void  SendMessToClient(int ID) {
  char* buffer = new char[DEFAULT_BUFLEN];
  int iResult;
  for (;; Sleep(75)) {
    if (Connections[ID]->is_closed) continue;
    memset(buffer, 0, sizeof(buffer));
    iResult = recv(Connections[ID]->connect, buffer, DEFAULT_BUFLEN, NULL);
    if ( iResult > 0 ){
      puts("Client message: " << clearMessage(buffer));
      for (size_t i = 0; i < Connections.size(); ++i) {
        Connections[i]->sendMessage(buffer);
      }
    } else {
      puts("Connection closed: client " << ID );
      // puts("recv failed: " << WSAGetLastError());
      CloseHandle(hThreads[ID]);
      Connections[ID]->close();
      break;
    }
  }
  delete[] buffer;
}

void printNetworkAddress(){
  char ac[80];
  if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR){
      putserr("Error " << WSAGetLastError() << " when getting local host name.");
      exit(1);
  }
  puts("Host name is " << ac << ".");

  // struct hostent *phe = gethostbyname(ac);
  // if (phe == 0){
  //     putserr("Bad host lookup.");
  //     exit(1);
  // }

  // for (int i = 0; phe->h_addr_list[i] != 0; ++i){
  //     struct in_addr addr;
  //     memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
  //     puts("Address " << i << ": " << inet_ntoa(addr));
  // }

  puts("ipconfig");
  system("ipconfig");

}
