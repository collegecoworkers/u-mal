#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <fstream>

#include <cstring>
#include <string>
#include <vector>

#include <WinSock2.h>
#include <WS2tcpip.h>

#define putserr(M) std::cerr << M << std::endl;
#define puts(M) std::cout << M << std::endl;
#define putsr(M) std::cout << M << "\r";
#define put(M) std::cout << M << "\r";
#define str_t std::string
#define str_npos std::string::npos
#define to_str(s) std::to_string(s)
#define vec_t std::vector

#define DEFAULT_BUFLEN 2048
#define DEFAULT_PORT "7770"
#define DELIMITER ";;;S"
#define THREADS_NUMBER 100000

class ItemConnect{
public:
  str_t name;
  SOCKET connect;
  bool is_closed = false;
  ItemConnect(SOCKET c);
  void close();
  void sendMessage(const str_t& buffer);
  ~ItemConnect();
};

HANDLE hThreads[THREADS_NUMBER];
SOCKET Connect;
vec_t<ItemConnect*> Connections;
SOCKET Listen;


void cleanup();
str_t clearMessage(const str_t&);
void  SendMessToClient(int);
void printNetworkAddress();
